BROWSERS=Firefox,Chrome
KARMA_OPTS=--colors --browsers $(BROWSERS)

BROWSERBUILD=./node_modules/.bin/browserbuild
UGLIFYJS=./node_modules/.bin/uglifyjs
KARMA=./node_modules/.bin/karma
FILES=`find src -type f`


all:
	$(BROWSERBUILD) -g ngLeaflet -b src/ -m ngLeaflet $(FILES) > lib/angular-leaflet.js
	$(UGLIFYJS) lib/angular-leaflet.js --mangle --output lib/angular-leaflet.min.js

setup:
	which bower > /dev/null || npm -g install bower
	npm install
	bower install

jshint:
	jshint src/

test:
	$(KARMA) start $(KARMA_OPTS) --single-run

karma:
	$(KARMA) start $(KARMA_OPTS) --auto-watch

