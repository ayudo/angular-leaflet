(function(){var global = this;function debug(){return debug};function require(p, parent){ var path = require.resolve(p) , mod = require.modules[path]; if (!mod) throw new Error('failed to require "' + p + '" from ' + parent); if (!mod.exports) { mod.exports = {}; mod.call(mod.exports, mod, mod.exports, require.relative(path), global); } return mod.exports;}require.modules = {};require.resolve = function(path){ var orig = path , reg = path + '.js' , index = path + '/index.js'; return require.modules[reg] && reg || require.modules[index] && index || orig;};require.register = function(path, fn){ require.modules[path] = fn;};require.relative = function(parent) { return function(p){ if ('debug' == p) return debug; if ('.' != p.charAt(0)) return require(p); var path = parent.split('/') , segs = p.split('/'); path.pop(); for (var i = 0; i < segs.length; i++) { var seg = segs[i]; if ('..' == seg) path.pop(); else if ('.' != seg) path.push(seg); } return require(path.join('/'), parent); };};require.register("ngLeaflet.js", function(module, exports, require, global){
'use strict';

module.exports = angular.module('ngLeaflet', [
  require('ngLeaflet/map/map').name,
  require('ngLeaflet/marker/marker').name,
  require('ngLeaflet/path/path').name
])
.service('lfHelper', require('ngLeaflet/helper-service'));

});require.register("ngLeaflet/helper-service.js", function(module, exports, require, global){
'use strict';

function LfHelperService() {
  this.toLatLng = function (position) {
    //if (angular.isArray(position)) {
      return [position[1], position[0]];
    //} else {
    //  return angular.copy(position);
    //}
  };

  this.updateLatLng = function (position, latlng) {
    if (latlng.lat !== position[1] && latlng.lng !== position[0]) {
      position[0] = latlng.lng;
      position[1] = latlng.lat;
    }
    //if (!latlng.equals(this.toLatLng(position))) {
    //  if (angular.isArray(position)) {
    //    position[0] = latlng.lng;
    //    position[1] = latlng.lat;
    //  } else {
    //    position.lng = latlng.lng;
    //    position.lat = latlng.lat;
    //  }
    //}
  };

  this.popupContent = function (object, transclude) {
    transclude(function (clone) {
      var html = clone.html();

      // tries to avoid empty popups
      if (clone.length > 1 || (html && html.trim() !== "")) {
        var div = angular.element('<div></div>').append(clone);
        object.bindPopup(div[0]);
      }
    });
  };
}

module.exports = LfHelperService;

});require.register("ngLeaflet/map/event-directives.js", function(module, exports, require, global){
'use strict';

var eventDirectives = {};
var eventTypes = 'click dblclick mousedown mouseup mouseover mouseout mousemove contextmenu focus blur preclick load unload viewreset movestart move moveend dragstart drag dragend zoomstart zoomend zoomlevelschange resize autopanstart layeradd layerremove baselayerchange overlayadd overlayremove locationfound locationerror popupopen popupclose'.split(' ');

eventTypes.forEach(function (type) {
  var name = 'lf' + type.charAt(0).toUpperCase() + type.slice(1);

  eventDirectives[name] = ['$parse', function ($parse) {
    return {
      restrict: 'A',
      require: ['?lfMarker', '?lfPolyline', '?lfPolygon', '?lfMap'],

      compile: function (_, attr) {
        var fn = $parse(attr[name]);

        return function (scope, element, _, controllers) {
          controllers.some(function (ctrl) {
            if (!ctrl) return;

            ctrl.get(function (object) {
              object.on(type, function (lfEvent) {
                var locals = { lfEvent: lfEvent };
                locals[ctrl.type()] = object;

                scope.$apply(function () {
                  fn(scope, locals);
                });
              });

              element.on('$destroy', function () {
                object.off(type);
              });
            });
          });
        };
      }
    };
  }];
});

module.exports = eventDirectives;

});require.register("ngLeaflet/map/map-controller.js", function(module, exports, require, global){
'use strict';

function LfMapCtrl($q, lfMap, lfHelper) {
  this.lfHelper = lfHelper;
  this.lfMap = lfMap;
  this.deferred = $q.defer();
}

LfMapCtrl.prototype.type = function () {
  return 'lfMap';
};

LfMapCtrl.prototype.init = function (name, map) {
  this.name = name;
  this.lfMap.add(name, map);
  this.deferred.resolve(map);
};

LfMapCtrl.prototype.get = function (fn) {
  if (fn) this.deferred.promise.then(fn);
  return this.deferred.promise;
};

LfMapCtrl.prototype.destroy = function () {
  if (this.name) {
    this.lfMap.destroy(this.name);
  }
};

LfMapCtrl.prototype.setTiles = function (options) {
  var tileLayer, self = this;

  switch (options.type) {
  case 'mapbox':
    tileLayer = L.mapbox.tileLayer(options.id, options);
    break;
  default:
    if (!angular.isObject(options)) {
      options = {
        url: 'http://{s}.tile.osm.org/{z}/{x}/{y}.png',
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      };
    }
    tileLayer = L.tileLayer(options.url, options);
  }

  self.get(function (map) {
    if (self.tileLayer) {
      map.removeLayer(self.tileLayer);
      self.tileLayer = null;
    }
    map.addLayer(tileLayer, true);
    self.tileLayer = tileLayer;
  });
};

LfMapCtrl.prototype.setCenter = function (center) {
  var self = this;
  if (angular.isObject(center)) {
    self.get(function (map) {
      map.setView(self.lfHelper.toLatLng(center.position), center.zoom || 14, center);
    });
  }
};

module.exports = ['$q', 'lfMap', 'lfHelper', LfMapCtrl];

});require.register("ngLeaflet/map/map-directive.js", function(module, exports, require, global){
'use strict';

function lfMapDirective(lfHelper) {
  function build(name, element) {
    var elem = angular.element('<div></div>').attr('id', name);
    element.after(elem);
    return elem[0];
  }

  return {
    restrict: 'AE',
    scope: {
      center: '=',
      tiles: '=?',
      options: '=?'
    },
    controller: 'lfMapCtrl',

    link: function (scope, element, attr, lfMapCtrl) {
      var name = attr.name || 'map';
      var map = L.map(build(name, element), scope.options);
      lfMapCtrl.init(name, map);

      scope.$watch('tiles',  angular.bind(lfMapCtrl, lfMapCtrl.setTiles),  true);
      scope.$watch('center', angular.bind(lfMapCtrl, lfMapCtrl.setCenter), true);

      map.on('moveend', function () {
        setTimeout(function () { // always run outside the angular loop
          var center = map.getCenter();
          scope.$apply(function () {
            lfHelper.updateLatLng(scope.center.position, center);
            scope.center.zoom = map.getZoom();
          });
        });
      });

      element.on('$destroy', function () {
        lfMapCtrl.destroy(name);
      });
    }
  };
}

module.exports = ['lfHelper', lfMapDirective];

});require.register("ngLeaflet/map/map-directive_test.js", function(module, exports, require, global){
require('ngLeaflet/map/map');

});require.register("ngLeaflet/map/map-service.js", function(module, exports, require, global){
'use strict';

function LfMapService($q) {
  var maps = {};
  var deferred = {};

  this.add = function (name, map) {
    if (maps[name]) {
      this.destroy(name);
    }
    maps[name] = map;

    if (deferred[name]) {
      deferred[name].resolve(map);
    }
  };

  this.get = function (name, fn) {
    if (!deferred[name]) deferred[name] = $q.defer();
    if (fn) deferred[name].promise.then(fn);
    return deferred[name].promise;
  };

  this.destroy = function (name) {
    maps[name].remove();
    delete maps[name];
    delete deferred[name];
  };
}

module.exports = ['$q', LfMapService];

});require.register("ngLeaflet/map/map-service_test.js", function(module, exports, require, global){
require('ngLeaflet/map/map');


});require.register("ngLeaflet/map/map.js", function(module, exports, require, global){
'use strict';

module.exports = angular.module('leaflet.map', [])
  .service('lfMap', require('ngLeaflet/map/map-service'))
  .controller('lfMapCtrl', require('ngLeaflet/map/map-controller'))
  .directive('lfMap', require('ngLeaflet/map/map-directive'));

var eventDirectives = require('ngLeaflet/map/event-directives');

for (var directiveName in eventDirectives) {
  if (eventDirectives.hasOwnProperty(directiveName)) {
    module.exports.directive(directiveName, eventDirectives[directiveName]);
  }
}

});require.register("ngLeaflet/marker/marker-controller.js", function(module, exports, require, global){
'use strict';

function LfMarkerCtrl($q) {
  this.deferred = $q.defer();
}

LfMarkerCtrl.prototype.type = function () {
  return 'lfMarker';
};

LfMarkerCtrl.prototype.init = function (marker) {
  this.marker = marker;
  this.deferred.resolve(marker);
};

LfMarkerCtrl.prototype.get = function (fn) {
  if (fn) this.deferred.promise.then(fn);
  return this.deferred.promise;
};

module.exports = ['$q', LfMarkerCtrl];

});require.register("ngLeaflet/marker/marker-directive.js", function(module, exports, require, global){
'use strict';

function lfMarkerDirective(lfHelper, $timeout, LEAFLET_ACCURACY_CIRCLE_STYLE) {
  return {
    transclude: true,
    restrict: 'AE',
    scope: {
      accuracy: '=?',
      draggable: '=?',
      focus: '=?',
      icon: '=?',
      position: '=',
      label: '=?',
      showLabel: '=?',
      popupState: '=?'
    },
    controller: 'lfMarkerCtrl',
    require: ['lfMarker', '^lfMap'],

    link: function (scope, element, attrs, ctrls, transclude) {
      var lfMarkerCtrl = ctrls[0],
          lfMapCtrl = ctrls[1];

      lfMapCtrl.get(function (map) {
        var marker = new L.marker([0, 0]),
            accuracy;
        lfHelper.popupContent(marker, transclude);

        if (scope.label) {
          marker.bindLabel(scope.label, { noHide: true });
        }

        marker.addTo(map);
        lfMarkerCtrl.init(marker);

        function update() {
          scope.$apply(function () {
            lfHelper.updateLatLng(scope.position, marker.getLatLng());
          });
        }

        // NOTE: this is hacking into Leaflet private API
        // FIXME: I have no reliable way to know that the icon element was created
        function copyStyle() {
          if (marker._icon) {
            marker._icon.style.cssText += ";" + (attrs.style || "");
          } else {
            $timeout(copyStyle, 10);
          }
        }

        scope.$watch('showLabel', function (showLabel) {
          if (showLabel) {
            marker.showLabel();
          } else {
            marker.hideLabel();
          }
        });

        scope.$watch('position', function (position) {
          if (position && position.length) {
            marker.setLatLng(lfHelper.toLatLng(position));
            if (accuracy) accuracy.setLatLng(lfHelper.toLatLng(position));
          }
        }, true);

        scope.$watch('icon', function (options) {
          var icon;

          if (options) {
            if (options.type === 'div') {
              icon = L.divIcon(options);
            } else if (options.type === 'default') {
              icon = new L.Icon.Default(options);
            } else {
              icon = L.icon(options);
            }
            marker.setIcon(icon);
            copyStyle();
            setDraggable();
          }
        }, true);

        scope.$watch('popupState', function (state) {
          if (state === 'closed') {
            marker.closePopup();
          }
        });

        // FIXME: delay to focus is required in some cases (eg: lfMarker was just
        //        inserted in DOM during the same angular loop), otherwise the
        //        popup is incorrectly rendered.
        //
        // TODO: delay until map is centered before opening popup, to avoid
        //       visual glitch while the map is moving.
        scope.$watch('focus', function (focus) {
          switch (focus) {
          case true:
            $timeout(function () { marker.openPopup(); });
            break;
          case false:
            marker.closePopup();
            break;
          }
        }, true);

        function setDraggable() {
          if (scope.draggable) {
            marker.dragging.enable();
            marker.on('drag', update);
            marker.on('dragend', update);
          } else {
            if (marker.dragging) marker.dragging.disable();
            marker.off('drag', update);
            marker.off('dragend', update);
          }
        }
        scope.$watch('draggable', setDraggable);

        scope.$watch('accuracy', function (radius) {
          if (radius == null) return;
          if (!accuracy) {
            var position = lfHelper.toLatLng(scope.position);
            accuracy = L.circle(position, radius, LEAFLET_ACCURACY_CIRCLE_STYLE).addTo(map);
          } else {
            accuracy.setRadius(radius);
          }
        }, true);

        attrs.$observe('style', copyStyle);

        attrs.$observe('zIndexOffset', function (value) {
          if (value != null) marker.setZIndexOffset(parseInt(value, 10));
        });

        element.on('$destroy', function () {
          map.removeLayer(marker);
          if (accuracy) map.removeLayer(accuracy);
          marker.off('drag', update);
          marker.off('dragend', update);
          marker.unbindPopup();
        });
      });
    }
  };
}

module.exports = ['lfHelper', '$timeout', 'LEAFLET_ACCURACY_CIRCLE_STYLE', lfMarkerDirective];

});require.register("ngLeaflet/marker/marker-directive_test.js", function(module, exports, require, global){
require('ngLeaflet/marker/marker');

});require.register("ngLeaflet/marker/marker.js", function(module, exports, require, global){
'use strict';

module.exports = angular.module('leaflet.marker', [])
  .constant('LEAFLET_ACCURACY_CIRCLE_STYLE', {})
  .controller('lfMarkerCtrl', require('ngLeaflet/marker/marker-controller'))
  .directive('lfMarker', require('ngLeaflet/marker/marker-directive'));

});require.register("ngLeaflet/path/path-controller.js", function(module, exports, require, global){
'use strict';

function LfPathCtrl($q) {
  this.deferred = $q.defer();
}

LfPathCtrl.prototype.type = function () {
  return 'lfPath';
};

LfPathCtrl.prototype.init = function (path) {
  this.path = path;
  this.deferred.resolve(path);
};

LfPathCtrl.prototype.get = function (fn) {
  if (fn) this.deferred.promise.then(fn);
  return this.deferred.promise;
};

module.exports = ['$q', LfPathCtrl];

});require.register("ngLeaflet/path/path.js", function(module, exports, require, global){
'use strict';

var buildPolyDirective = require('ngLeaflet/path/poly-directive');

module.exports = angular.module('leaflet.path', [])
  .controller('lfPathCtrl', require('ngLeaflet/path/path-controller'))
  .directive('lfPolyline', buildPolyDirective('polyline'))
  .directive('lfPolygon',  buildPolyDirective('polygon'));

});require.register("ngLeaflet/path/poly-directive.js", function(module, exports, require, global){
'use strict';

module.exports = function (type) {
  var constructor = L[type];
  var camelName = type[0].toUpperCase() + type.slice(1);

  return ['lfHelper', function polyDirective(lfHelper) {
    return {
      transclude: true,
      restrict: 'AE',
      scope: {
        positions: '=',
        focus: '=?',
        options: '=?',
        style: '=?pathStyle'
      },
      controller: 'lfPathCtrl',
      require: ['lf' + camelName, '^lfMap'],

      link: function (scope, element, _, ctrls, transclude) {
        var lfPathCtrl = ctrls[0],
            lfMapCtrl = ctrls[1];

        lfMapCtrl.get(function (map) {
          var vector = constructor([], scope.options);
          lfHelper.popupContent(vector, transclude);

          vector.addTo(map);
          lfPathCtrl.init(vector);

          scope.$watch('positions', function (positions) {
            if (!positions) return;
            vector.setLatLngs(positions.map(lfHelper.toLatLng));
          }, true);

          scope.$watch('style', function (styles) {
            if (!styles) return;
            vector.setStyle(styles);
          }, true);

          scope.$watch('focus', function (focus) {
            var method = focus ? 'openPopup' : 'closePopup';
            vector[method]();
          });

          element.on('$destroy', function () {
            map.removeLayer(vector);
          });
        });
      }
    };
  }];
};

});var exp = require('ngLeaflet');if ("undefined" != typeof module) module.exports = exp;else ngLeaflet = exp;
})();
