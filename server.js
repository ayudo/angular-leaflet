/* jshint node: true */
'use strict';

var connect = require('connect');

var app = connect()
  .use(connect.logger('dev'))
  .use(connect.static(__dirname))
  .use(connect.compress())
  .use(function (_, response) {
    response.statusCode = 302;
    response.setHeader('Location', '/examples/demo.html');
    response.setHeader('Content-Length', '0');
    response.end();
  });

var port = process.env.PORT || 3000;
connect.createServer(app).listen(port);

