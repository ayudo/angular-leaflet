var tests = [];

for (var file in window.__karma__.files) {
  if (window.__karma__.files.hasOwnProperty(file) && /_test\.js$/.test(file)) {
    tests.push(file);
  }
}

requirejs.config({
  baseUrl: '/base/src',
  paths: {
    leaflet: '../vendor/leaflet/dist/leaflet-src'
  }
});

// NOTE: fixes a leaflet bug (anonymous define of object) with requirejs/cajon
require(['leaflet'], function (L) {
  window.L = L;
  require(tests, window.__karma__.start);
});
