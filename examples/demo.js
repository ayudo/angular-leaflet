define(['angular', 'leaflet', 'ngLeaflet'], function (angular, L) {
  'use strict';

  window.L = L;
  L.Icon.Default.imagePath = '../vendor/leaflet/dist/images/';

  var id = 0;

  function DemoCtrl() {
    this.center = {
      position: [2.3499584, 48.8689775],
      zoom: 14
    };

    this.markers = [
      { id: id += 1, position: [2.3508059, 48.8664509] },
      { id: id += 1, position: [2.3471903, 48.8551539] }
    ];

    this.polyline = [
      [2.3550224, 48.8800702],
      [2.3586273, 48.8792235],
      [2.3578763, 48.8770926],
      [2.3608160, 48.8759919],
      [2.3628544, 48.8742842],
      [2.3678541, 48.8689210],
      [2.3692059, 48.8672202]
    ];

    this.polygon = [
      [2.3461496, 48.8627808],
      [2.3487031, 48.8638466],
      [2.3535740, 48.8612704],
      [2.3521792, 48.8572258],
      [2.3467290, 48.8583128]
    ];

    this.editable = false;
  }

  DemoCtrl.prototype.focusMarker = function (marker) {
    this.center.position = angular.copy(marker.position);
    this.center.zoom = 16;
  };

  DemoCtrl.prototype.toggleEditState = function () {
    this.editable = !this.editable;
  };

  DemoCtrl.prototype.createMarker = function (lfEvent) {
    if (this.editable) {
      this.markers.push({
        id: id += 1,
        position: [lfEvent.latlng.lng, lfEvent.latlng.lat]
      });
    }
  };

  DemoCtrl.prototype.destroyMarker = function (marker) {
    var idx = this.markers.indexOf(marker);
    if (idx > -1) this.markers.splice(idx, 1);
  };

  return angular.module('demo', ['ngLeaflet']).controller('DemoCtrl', DemoCtrl);
});
