module.exports = function(config) {
  config.set({
    frameworks: ['mocha', 'cajon'],
    files: [
      { pattern: 'src/**/*.js', included: false },
      { pattern: 'vendor/angular/angular.js', included: true, watched: false },
      { pattern: 'vendor/leaflet/dist/leaflet-src.js', included: false, watched: false },
      'test-main.js'
    ]
  });
};
