'use strict';

module.exports = angular.module('leaflet.map', [])
  .service('lfMap', require('ngLeaflet/map/map-service'))
  .controller('lfMapCtrl', require('ngLeaflet/map/map-controller'))
  .directive('lfMap', require('ngLeaflet/map/map-directive'));

var eventDirectives = require('ngLeaflet/map/event-directives');

for (var directiveName in eventDirectives) {
  if (eventDirectives.hasOwnProperty(directiveName)) {
    module.exports.directive(directiveName, eventDirectives[directiveName]);
  }
}
