'use strict';

function lfMapDirective(lfHelper) {
  function build(name, element) {
    var elem = angular.element('<div></div>').attr('id', name);
    element.after(elem);
    return elem[0];
  }

  return {
    restrict: 'AE',
    scope: {
      center: '=',
      tiles: '=?',
      options: '=?'
    },
    controller: 'lfMapCtrl',

    link: function (scope, element, attr, lfMapCtrl) {
      var name = attr.name || 'map';
      var map = L.map(build(name, element), scope.options);
      lfMapCtrl.init(name, map);

      scope.$watch('tiles',  angular.bind(lfMapCtrl, lfMapCtrl.setTiles),  true);
      scope.$watch('center', angular.bind(lfMapCtrl, lfMapCtrl.setCenter), true);

      map.on('moveend', function () {
        setTimeout(function () { // always run outside the angular loop
          var center = map.getCenter();
          scope.$apply(function () {
            lfHelper.updateLatLng(scope.center.position, center);
            scope.center.zoom = map.getZoom();
          });
        });
      });

      element.on('$destroy', function () {
        lfMapCtrl.destroy(name);
      });
    }
  };
}

module.exports = ['lfHelper', lfMapDirective];
