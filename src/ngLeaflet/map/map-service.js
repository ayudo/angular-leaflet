'use strict';

function LfMapService($q) {
  var maps = {};
  var deferred = {};

  this.add = function (name, map) {
    if (maps[name]) {
      this.destroy(name);
    }
    maps[name] = map;

    if (deferred[name]) {
      deferred[name].resolve(map);
    }
  };

  this.get = function (name, fn) {
    if (!deferred[name]) deferred[name] = $q.defer();
    if (fn) deferred[name].promise.then(fn);
    return deferred[name].promise;
  };

  this.destroy = function (name) {
    maps[name].remove();
    delete maps[name];
    delete deferred[name];
  };
}

module.exports = ['$q', LfMapService];
