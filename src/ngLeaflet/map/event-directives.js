'use strict';

var eventDirectives = {};
var eventTypes = 'click dblclick mousedown mouseup mouseover mouseout mousemove contextmenu focus blur preclick load unload viewreset movestart move moveend dragstart drag dragend zoomstart zoomend zoomlevelschange resize autopanstart layeradd layerremove baselayerchange overlayadd overlayremove locationfound locationerror popupopen popupclose'.split(' ');

eventTypes.forEach(function (type) {
  var name = 'lf' + type.charAt(0).toUpperCase() + type.slice(1);

  eventDirectives[name] = ['$parse', function ($parse) {
    return {
      restrict: 'A',
      require: ['?lfMarker', '?lfPolyline', '?lfPolygon', '?lfMap'],

      compile: function (_, attr) {
        var fn = $parse(attr[name]);

        return function (scope, element, _, controllers) {
          controllers.some(function (ctrl) {
            if (!ctrl) return;

            ctrl.get(function (object) {
              object.on(type, function (lfEvent) {
                var locals = { lfEvent: lfEvent };
                locals[ctrl.type()] = object;

                scope.$apply(function () {
                  fn(scope, locals);
                });
              });

              element.on('$destroy', function () {
                object.off(type);
              });
            });
          });
        };
      }
    };
  }];
});

module.exports = eventDirectives;
