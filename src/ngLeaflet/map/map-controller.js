'use strict';

function LfMapCtrl($q, lfMap, lfHelper) {
  this.lfHelper = lfHelper;
  this.lfMap = lfMap;
  this.deferred = $q.defer();
}

LfMapCtrl.prototype.type = function () {
  return 'lfMap';
};

LfMapCtrl.prototype.init = function (name, map) {
  this.name = name;
  this.lfMap.add(name, map);
  this.deferred.resolve(map);
};

LfMapCtrl.prototype.get = function (fn) {
  if (fn) this.deferred.promise.then(fn);
  return this.deferred.promise;
};

LfMapCtrl.prototype.destroy = function () {
  if (this.name) {
    this.lfMap.destroy(this.name);
  }
};

LfMapCtrl.prototype.setTiles = function (options) {
  var tileLayer, self = this;

  switch (options && options.type) {
  case 'mapbox':
    tileLayer = L.mapbox.tileLayer(options.id, options);
    break;
  default:
    if (!angular.isObject(options)) {
      options = {
        url: 'http://{s}.tile.osm.org/{z}/{x}/{y}.png',
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      };
    }
    tileLayer = L.tileLayer(options.url, options);
  }

  self.get(function (map) {
    if (self.tileLayer) {
      map.removeLayer(self.tileLayer);
      self.tileLayer = null;
    }
    map.addLayer(tileLayer, true);
    self.tileLayer = tileLayer;
  });
};

LfMapCtrl.prototype.setCenter = function (center) {
  var self = this;
  if (angular.isObject(center)) {
    self.get(function (map) {
      map.setView(self.lfHelper.toLatLng(center.position), center.zoom || 14, center);
    });
  }
};

module.exports = ['$q', 'lfMap', 'lfHelper', LfMapCtrl];
