'use strict';

function LfMarkerCtrl($q) {
  this.deferred = $q.defer();
}

LfMarkerCtrl.prototype.type = function () {
  return 'lfMarker';
};

LfMarkerCtrl.prototype.init = function (marker) {
  this.marker = marker;
  this.deferred.resolve(marker);
};

LfMarkerCtrl.prototype.get = function (fn) {
  if (fn) this.deferred.promise.then(fn);
  return this.deferred.promise;
};

module.exports = ['$q', LfMarkerCtrl];
