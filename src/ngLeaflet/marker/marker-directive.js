'use strict';

function lfMarkerDirective(lfHelper, $timeout, LEAFLET_ACCURACY_CIRCLE_STYLE) {
  return {
    transclude: true,
    restrict: 'AE',
    scope: {
      accuracy: '=?',
      draggable: '=?',
      focus: '=?',
      icon: '=?',
      position: '=',
      label: '=?',
      showLabel: '=?',
      popupState: '=?'
    },
    controller: 'lfMarkerCtrl',
    require: ['lfMarker', '^lfMap'],

    link: function (scope, element, attrs, ctrls, transclude) {
      var lfMarkerCtrl = ctrls[0],
          lfMapCtrl = ctrls[1];

      lfMapCtrl.get(function (map) {
        var marker = new L.marker([0, 0]),
            accuracy;
        lfHelper.popupContent(marker, transclude);

        if (scope.label) {
          marker.bindLabel(scope.label, { noHide: true });
        }

        marker.addTo(map);
        lfMarkerCtrl.init(marker);

        function update() {
          scope.$apply(function () {
            lfHelper.updateLatLng(scope.position, marker.getLatLng());
          });
        }

        // NOTE: this is hacking into Leaflet private API
        // FIXME: I have no reliable way to know that the icon element was created
        function copyStyle() {
          if (marker._icon) {
            marker._icon.style.cssText += ";" + (attrs.style || "");
          } else {
            $timeout(copyStyle, 10);
          }
        }

        if (marker.showLabel && marker.hideLabel) {
          scope.$watch('showLabel', function (showLabel) {
            if (showLabel) {
              marker.showLabel();
            } else {
              marker.hideLabel();
            }
          });
        }

        scope.$watch('position', function (position) {
          if (position && position.length) {
            marker.setLatLng(lfHelper.toLatLng(position));
            if (accuracy) accuracy.setLatLng(lfHelper.toLatLng(position));
          }
        }, true);

        scope.$watch('icon', function (options) {
          var icon;

          if (options) {
            if (options.type === 'div') {
              icon = L.divIcon(options);
            } else if (options.type === 'default') {
              icon = new L.Icon.Default(options);
            } else {
              icon = L.icon(options);
            }
            marker.setIcon(icon);
            copyStyle();
            setDraggable();
          }
        }, true);

        scope.$watch('popupState', function (state) {
          if (state === 'closed') {
            marker.closePopup();
          }
        });

        // FIXME: delay to focus is required in some cases (eg: lfMarker was just
        //        inserted in DOM during the same angular loop), otherwise the
        //        popup is incorrectly rendered.
        //
        // TODO: delay until map is centered before opening popup, to avoid
        //       visual glitch while the map is moving.
        scope.$watch('focus', function (focus) {
          switch (focus) {
          case true:
            $timeout(function () { marker.openPopup(); });
            break;
          case false:
            marker.closePopup();
            break;
          }
        }, true);

        function setDraggable() {
          if (scope.draggable) {
            marker.dragging.enable();
            marker.on('drag', update);
            marker.on('dragend', update);
          } else {
            if (marker.dragging) marker.dragging.disable();
            marker.off('drag', update);
            marker.off('dragend', update);
          }
        }
        scope.$watch('draggable', setDraggable);

        scope.$watch('accuracy', function (radius) {
          if (radius == null) return;
          if (!accuracy) {
            var position = lfHelper.toLatLng(scope.position);
            accuracy = L.circle(position, radius, LEAFLET_ACCURACY_CIRCLE_STYLE).addTo(map);
          } else {
            accuracy.setRadius(radius);
          }
        }, true);

        attrs.$observe('style', copyStyle);

        attrs.$observe('zIndexOffset', function (value) {
          if (value != null) marker.setZIndexOffset(parseInt(value, 10));
        });

        element.on('$destroy', function () {
          map.removeLayer(marker);
          if (accuracy) map.removeLayer(accuracy);
          marker.off('drag', update);
          marker.off('dragend', update);
          marker.unbindPopup();
        });
      });
    }
  };
}

module.exports = ['lfHelper', '$timeout', 'LEAFLET_ACCURACY_CIRCLE_STYLE', lfMarkerDirective];
