'use strict';

module.exports = angular.module('leaflet.marker', [])
  .constant('LEAFLET_ACCURACY_CIRCLE_STYLE', {})
  .controller('lfMarkerCtrl', require('ngLeaflet/marker/marker-controller'))
  .directive('lfMarker', require('ngLeaflet/marker/marker-directive'));
