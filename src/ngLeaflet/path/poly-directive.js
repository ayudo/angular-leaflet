'use strict';

module.exports = function (type) {
  var constructor = L[type];
  var camelName = type[0].toUpperCase() + type.slice(1);

  return ['lfHelper', function polyDirective(lfHelper) {
    return {
      transclude: true,
      restrict: 'AE',
      scope: {
        positions: '=',
        focus: '=?',
        options: '=?',
        style: '=?pathStyle'
      },
      controller: 'lfPathCtrl',
      require: ['lf' + camelName, '^lfMap'],

      link: function (scope, element, _, ctrls, transclude) {
        var lfPathCtrl = ctrls[0],
            lfMapCtrl = ctrls[1];

        lfMapCtrl.get(function (map) {
          var vector = constructor([], scope.options);
          lfHelper.popupContent(vector, transclude);

          vector.addTo(map);
          lfPathCtrl.init(vector);

          scope.$watch('positions', function (positions) {
            if (!positions) return;
            vector.setLatLngs(positions.map(lfHelper.toLatLng));
          }, true);

          scope.$watch('style', function (styles) {
            if (!styles) return;
            vector.setStyle(styles);
          }, true);

          scope.$watch('focus', function (focus) {
            var method = focus ? 'openPopup' : 'closePopup';
            vector[method]();
          });

          element.on('$destroy', function () {
            map.removeLayer(vector);
          });
        });
      }
    };
  }];
};
