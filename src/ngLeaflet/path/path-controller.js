'use strict';

function LfPathCtrl($q) {
  this.deferred = $q.defer();
}

LfPathCtrl.prototype.type = function () {
  return 'lfPath';
};

LfPathCtrl.prototype.init = function (path) {
  this.path = path;
  this.deferred.resolve(path);
};

LfPathCtrl.prototype.get = function (fn) {
  if (fn) this.deferred.promise.then(fn);
  return this.deferred.promise;
};

module.exports = ['$q', LfPathCtrl];
