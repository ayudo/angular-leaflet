'use strict';

var buildPolyDirective = require('ngLeaflet/path/poly-directive');

module.exports = angular.module('leaflet.path', [])
  .controller('lfPathCtrl', require('ngLeaflet/path/path-controller'))
  .directive('lfPolyline', buildPolyDirective('polyline'))
  .directive('lfPolygon',  buildPolyDirective('polygon'));
