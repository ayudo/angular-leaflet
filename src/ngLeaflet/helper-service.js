'use strict';

function LfHelperService() {
  this.toLatLng = function (position) {
    //if (angular.isArray(position)) {
      return [position[1], position[0]];
    //} else {
    //  return angular.copy(position);
    //}
  };

  this.updateLatLng = function (position, latlng) {
    if (latlng.lat !== position[1] && latlng.lng !== position[0]) {
      position[0] = latlng.lng;
      position[1] = latlng.lat;
    }
    //if (!latlng.equals(this.toLatLng(position))) {
    //  if (angular.isArray(position)) {
    //    position[0] = latlng.lng;
    //    position[1] = latlng.lat;
    //  } else {
    //    position.lng = latlng.lng;
    //    position.lat = latlng.lat;
    //  }
    //}
  };

  this.popupContent = function (object, transclude) {
    transclude(function (clone) {
      var html = clone.html();

      // tries to avoid empty popups
      if (clone.length > 1 || (html && html.trim() !== "")) {
        var div = angular.element('<div></div>').append(clone);
        object.bindPopup(div[0]);
      }
    });
  };
}

module.exports = LfHelperService;
