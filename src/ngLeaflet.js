'use strict';

module.exports = angular.module('ngLeaflet', [
  require('ngLeaflet/map/map').name,
  require('ngLeaflet/marker/marker').name,
  require('ngLeaflet/path/path').name
])
.service('lfHelper', require('ngLeaflet/helper-service'));
