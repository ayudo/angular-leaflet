requirejs.config({
  baseUrl: '../src',
  paths: {
    angular: '../vendor/angular/angular',
    leaflet: '../vendor/leaflet/dist/leaflet-src'
  },
  shim: {
    angular: { exports: 'angular' }
  }
});
